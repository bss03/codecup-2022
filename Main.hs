module Main (main) where

{-
Per the [technical rules](https://www.codecup.nl/rules_tech.php), "The source code must consist of
one single file.", so Main.hs cannnot import anything except packages shipped with GHC.  Other
files can freely import Main, though.
-}

-- base

-- transformers
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except (ExceptT (ExceptT), runExceptT, throwE)
import Data.Bifunctor (first)
import qualified Data.Either as Either
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Semigroup (Semigroup ((<>)))
import Data.Void (absurd)
import System.IO (hFlush, hPutStrLn, stderr)
import Prelude hiding (Left, Right, init, log)

data RowIndex = Ra | Rb | Rc | Rd | Re | Rf | Rg | Rh | Ri
  deriving (Eq, Ord, Show, Read, Enum, Bounded)

charRow :: Char -> Maybe RowIndex
charRow 'a' = Just Ra
charRow 'b' = Just Rb
charRow 'c' = Just Rc
charRow 'd' = Just Rd
charRow 'e' = Just Re
charRow 'f' = Just Rf
charRow 'g' = Just Rg
charRow 'h' = Just Rh
charRow 'i' = Just Ri
charRow _ = Nothing

rowChar :: RowIndex -> Char
rowChar Ra = 'a'
rowChar Rb = 'b'
rowChar Rc = 'c'
rowChar Rd = 'd'
rowChar Re = 'e'
rowChar Rf = 'f'
rowChar Rg = 'g'
rowChar Rh = 'h'
rowChar Ri = 'i'

data ColumnIndex = Ca | Cb | Cc | Cd | Ce | Cf | Cg
  deriving (Eq, Ord, Show, Read, Enum, Bounded)

charColumn :: Char -> Maybe ColumnIndex
charColumn 'a' = Just Ca
charColumn 'b' = Just Cb
charColumn 'c' = Just Cc
charColumn 'd' = Just Cd
charColumn 'e' = Just Ce
charColumn 'f' = Just Cf
charColumn 'g' = Just Cg
charColumn _ = Nothing

columnChar :: ColumnIndex -> Char
columnChar Ca = 'a'
columnChar Cb = 'b'
columnChar Cc = 'c'
columnChar Cd = 'd'
columnChar Ce = 'e'
columnChar Cf = 'f'
columnChar Cg = 'g'

data Tile = Left | Straight | Right
  deriving (Eq, Ord, Show, Read, Enum, Bounded)

charTile :: Char -> Maybe Tile
charTile 'l' = Just Left
charTile 's' = Just Straight
charTile 'r' = Just Right
charTile _ = Nothing

tileChar :: Tile -> Char
tileChar Left = 'l'
tileChar Straight = 's'
tileChar Right = 'r'

data Move = MkMove {row :: !RowIndex, column :: !ColumnIndex, tile :: !Tile}
  deriving (Eq, Show, Read)

data InvalidMoveContent = InvalidRow Char | InvalidColumn Char | InvalidTile Char
  deriving (Eq, Show, Read)

data InvalidMove = InvalidMoveLength | InvalidMoveContents (NonEmpty InvalidMoveContent)
  deriving (Eq, Show, Read)

newtype Validation e a = MkValidation {unValidation :: Either e a}
  deriving (Eq, Show, Read)

instance Functor (Validation e) where
  fmap _ (MkValidation (Either.Left e)) = MkValidation $ Either.Left e
  fmap f (MkValidation (Either.Right x)) = MkValidation . Either.Right $ f x

instance Semigroup e => Applicative (Validation e) where
  pure = MkValidation . Either.Right
  MkValidation (Either.Left ef) <*> MkValidation (Either.Left ex) =
    MkValidation . Either.Left $ ef <> ex
  MkValidation (Either.Left ef) <*> MkValidation (Either.Right _) =
    MkValidation . Either.Left $ ef
  MkValidation (Either.Right _) <*> MkValidation (Either.Left ex) =
    MkValidation . Either.Left $ ex
  MkValidation (Either.Right f) <*> MkValidation (Either.Right x) =
    MkValidation . Either.Right $ f x

parseMove :: String -> Either InvalidMove Move
parseMove [r, c, t] = first InvalidMoveContents . unValidation $ MkMove <$> vr <*> vc <*> vt
  where
    validate good bad x = MkValidation . maybe (Either.Left $ bad x :| []) Either.Right $ good x
    vr = validate charRow InvalidRow r
    vc = validate charColumn InvalidColumn c
    vt = validate charTile InvalidTile t
parseMove _ = Either.Left InvalidMoveLength

printMove :: Move -> String
printMove MkMove {row = r, column = c, tile = t} = [rowChar r, columnChar c, tileChar t]

data Input = IMove !Move | Start | Quit
  deriving (Eq, Show, Read)

type InvalidInput = InvalidMove

lineInput :: String -> Either InvalidInput Input
lineInput "Start" = Either.Right Start
lineInput "Quit" = Either.Right Quit
lineInput move = IMove <$> parseMove move

type Output = Move

outputLine :: Output -> String
outputLine = printMove

type Init m = Move -> Move -> m (Strats m)

data Strats m
  = MkStrats
      { blue :: !(Strategy m),
        red :: !(Step m)
      }

instance Show (Strats m) where
  show MkStrats {blue = b} = "MkStrats {blue = " ++ shows b "}"

data Strategy m
  = MkStrat
      { play :: !Move,
        next :: !(Step m)
      }

instance Show (Strategy m) where
  show MkStrat {play = p} = "MkStrat {play = " ++ shows p "}"

type Step m = Move -> m (Strategy m)

playerId :: String
playerId = "Haskell codecup-2022"

log :: String -> IO ()
log = hPutStrLn stderr

logPrint :: Show a => a -> IO ()
logPrint = log . show

constStrats :: Monad m => Move -> Strats m
constStrats x = MkStrats {blue = strat, red = step}
  where
    strat = MkStrat {play = x, next = step}
    step _ = return strat

data GameEnd = BadInput InvalidInput | UnexpectedStart | UnexpectedQuit | GameOver
  deriving (Eq, Show, Read)

input :: ExceptT GameEnd IO Input
input = ExceptT $ first BadInput . lineInput <$> getLine

inputMove :: ExceptT GameEnd IO Move
inputMove = do
  i <- input
  ExceptT . return $ case i of
    IMove m -> Either.Right m
    Start -> Either.Left UnexpectedStart
    Quit -> Either.Left UnexpectedQuit

initPlay :: Init IO -> ExceptT GameEnd IO a
initPlay init = do
  m1 <- inputMove
  m2 <- inputMove
  strats <- lift $ init m1 m2
  stratsPlay strats

stratsPlay :: Strats IO -> ExceptT GameEnd IO a
stratsPlay strats = do
  i <- input
  case i of
    IMove m -> do
      strat <- lift $ do
        log $ "OPP: " ++ show m
        log "Playing (second) as Red."
        red strats m
      stratPlay strat
    Start -> do
      lift $ log "Playing (first) as Blue."
      stratPlay (blue strats)
    Quit -> throwE GameOver

stratPlay :: Strategy IO -> ExceptT GameEnd IO a
stratPlay strat = do
  lift $ do
    logPrint p
    putStrLn $ outputLine p
  i <- input
  case i of
    IMove m -> do
      nextStrat <- lift $ do
        log $ "OPP: " ++ show m
        next strat m
      stratPlay nextStrat
    Start -> throwE UnexpectedStart
    Quit -> throwE GameOver
  where
    p = play strat

main :: IO ()
main = do
  hPutStrLn stderr playerId
  hFlush stderr
  end <- runExceptT $ initPlay (\_ _ -> return . constStrats $ MkMove {row = Ra, column = Ca, tile = Straight})
  case end of
    Either.Left go -> logPrint go
    Either.Right x -> return $ absurd x
  hFlush stderr
